package teste1;


import static org.junit.Assert.assertEquals;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class alter {
	
	
	@Test
	public void testando() {
		WebDriverManager.chromedriver().setup(); 
		WebDriver driver = new ChromeDriver();
		driver.get("https://testpages.herokuapp.com/styled/basic-html-form-test.html");
		
		
		WebElement userNameBox = driver.findElement(By.name("username")); 
		WebElement passWordBox = driver.findElement(By.name("password"));
		WebElement commentsBox = driver.findElement(By.name("comments"));
		
		WebElement radio1 = driver.findElement(By.xpath("//input[ @value='rd1']")); 
		
		WebElement checkBox1 = driver.findElement(By.xpath("//input[ @value= 'cb1']"));
		WebElement checkBox3 = driver.findElement(By.xpath("//input[ @value= 'cb3']"));
		
		WebElement selection1 = driver.findElement(By.xpath("//option [@value = 'ms1']")); 
		WebElement selection4 = driver.findElement(By.xpath("//option [@value = 'ms4']")); 
		
		WebElement submitButton = driver.findElement(By.xpath("//input[ @value = 'submit']"));
		

		radio1.click();
		checkBox1.click();
		checkBox3.click();
		selection1.click();
		selection4.click();
		commentsBox.clear();
		submitButton.click();
		
		Assert.assertEquals("No Value for username",driver.findElement(By.xpath("/html/body/div/div[3]/p[1]/strong")).getText());
		Assert.assertEquals("No Value for password",driver.findElement(By.xpath("/html/body/div/div[3]/p[2]/strong")).getText());
		Assert.assertEquals("No Value for comments",driver.findElement(By.xpath("/html/body/div/div[3]/p[3]/strong")).getText());
	}

}
